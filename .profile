export QT_QPA_PLATFORMTHEME="qt5ct"
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
# fix "xdg-open fork-bomb" export your preferred browser from here
export BROWSER='/usr/bin/firefox'

export EDITOR="nvim"         # $EDITOR opens in terminal
export VISUAL="emacs"        # $VISUAL opens in GUI mode

# My custom path vars
export PATH=~/.bin$(yarn global bin):~/.local/bin:~/.i3/bin:/opt/texlive/2020/bin/x86_64-linux:$PATH
export MANPATH=/opt/texlive/2020/texmf-dist/doc/man:$MANPATH
export INFOPATH=/opt/texlive/2020/texmf-dist/doc/info:$INFOPATH
