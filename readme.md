# My I3 DotFile Setup

**WARNING:** This repository is preparing to be moved to 
myriacore-dotfiles>dotfiles! When this happens, myriacore-dotfiles>i3 will be 
turned into a submodule with the contents of [`/.i3`](/.i3).

## System Requirements

Must have the following programs installed:

- [firefox](https://www.archlinux.org/packages/?name=firefox), used as the main
  browser

- [nemo](https://www.archlinux.org/packages/?name=nemo), used as the main file
  browser

- [emacs](https://aur.archlinux.org/packages/emacs-git), 
  [vscode](https://www.archlinux.org/packages/?name=code), 
  [fvim](http://aur.archlinux.org/packages/fvim) 
  and [nvim](https://www.archlinux.org/packages/?name=neovim) 
  are used as the main file editors. 
  - Emacs dotfiles are tracked from myriacore-dotfiles/emacs>.
  - FVim and NVim dotfiles are tracked from myriacore-dotfiles/neovim>.
  - Customizations / extensions are included in 
    [`/.vscode-oss`](https://gitlab.com/tooling-and-env/vscode-dotfiles) and 
    [`/.config/Code - OSS`](https://gitlab.com/tooling-and-env/vscode-dotfiles).

- [flameshot](https://www.archlinux.org/packages/?name=flameshot) for the
  <kbd>PrtSc</kbd> screenshot menu

- [dmenu-manjaro](https://gitlab.manjaro.org/packages/community/dmenu-manjaro),
  a dmenu used by many OS facilities.

- [ulauncher](https://aur.archlinux.org/packages/ulauncher/) as the main app
  launcher. 

<!-- 
Consider switching to rofi for the above use case
https://www.archlinux.org/packages/community/x86_64/rofi/
-->

- [xfce4-terminal](https://www.archlinux.org/packages/?name=xfce4-terminal),
  used for the <kbd>Super-Enter</kbd> dropdown terminal.

- [gnome-system-monitor](https://gitlab.gnome.org/GNOME/gnome-system-monitor)
  [gnome-system-log](https://www.archlinux.org/packages?name=gnome-system-log)
  [manjaro-settings-manager](https://gitlab.manjaro.org/applications/manjaro-settings-manager)
  for the <kbd>Super-Ctrl-B</kbd> settings menu.

- [ffmpeg](https://www.archlinux.org/packages/?name=ffmpeg) and
  [slop](https://www.archlinux.org/packages/?name=slop) for the
  <kbd>Super-PrtSc</kbd> screen record dialog

- [autorandr](https://www.archlinux.org/packages/?name=autorandr) for display
  detection

- [pulseaudio](https://www.archlinux.org/packages/?name=pulseaudio),
  [pulseaudio-alsa](https://www.archlinux.org/packages/?name=pulseaudio-alsa),
  [pulseaudio-bluetooth](https://www.archlinux.org/packages/?name=pulseaudio-bluetooth),
  and [pavucontrol](httpshttps://www.archlinux.org/packages/?name=pavucontrol),
  all for audio control

- [twemoji](https://aur.archlinux.org/packages/ttf-twemoji/), for emoji fonts
  xP

- [zenity](https://www.archlinux.org/packages/?name=zenity),
  [xsel](https://www.archlinux.org/packages/?name=xsel), and
  [xclip](https://www.archlinux.org/packages/?name=xclip), to be used by the
  [texpander script](https://gitlab.com/myriacore/texpander) located in
  [`~/.texpander/.bin/texpander.sh`](https://gitlab.com/tooling-and-env/texpander-dotfiles).
  [dmenu](https://tools.suckless.org/dmenu/) has already been mentioned, but is
  also required by texpander. Texpander is used for the
  <kbd>Super-&lt;period&gt;</kbd> text replacement feature.

- [feh](https://www.archlinux.org/packages/?name=feh) for displaying pictures,
  as is done with the weather i3blocklet

- [wget](https://www.archlinux.org/packages/?name=wget), used to download
  pictures for the weather i3blocklet

- [jq](https://www.archlinux.org/packages/?name=jq) and
  [yq](https://www.archlinux.org/packages/?name=yq) to be used by the texpander
  configuration scripts in
  [`~/.texpander/.config/`](https://gitlab.com/myriacore-dotfiles/texpander).

- [xclip](https://www.archlinux.org/packages/?name=xclip) is also used by the
  bash script functions `copy` and `paste`.

- [xkill](https://www.archlinux.org/packages/?name=xorg-xkill), used to kill
  focused windows with <kbd>Super-Ctrl-X</kbd>

- [pacman](https://www.archlinux.org/pacman/), the package manager of choice,
  and [pamac](https://gitlab.manjaro.org/applications/pamac), which provides a
  nice GUI and systray applet for pacman.

- [pulsemixer](https://www.archlinux.org/packages/?name=pulsemixer), for audio
  control of PulseAudio, and for the volume i3blocklet. 

- [clipit](https://github.com/CristianHenzel/ClipIt), the applet used as a
  clipboard manager

- [i3blocks](https://www.archlinux.org/packages/?name=i3blocks), used to make
  the applets next to the systray. 

- [i3-gaps](https://www.archlinux.org/packages/?name=i3-gaps)

- [libinput-gestures](https://aur.archlinux.org/packages/libinput-gestures/)
  for touchpad gesture recognition

- [deadd-notification-center](https://aur.archlinux.org/packages/deadd-notification-center/)
  to replace dunst as my notification daemon, and to achieve the notification
  center. Technically, I'm using [my own
  PKGBUILD](https://github.com/phuhl/linux_notification_center/blob/d75e57e9d14dc6b828c41957c2316ad6de613e76/pkg-git/PKGBUILD)
  to have access to the more recent features that I built into it. When 
  [linux_notification_center#92](https://github.com/phuhl/linux_notification_center/pull/92)
  is merged, I'll just be able to use 
  [deadd-notification-center-git](https://aur.archlinux.org/packages/deadd-notification-center-git/).


- [notify-send.sh](https://aur.archlinux.org/packages/notify-send.sh) for the
  notifications sent by volume buttons and screen brightness controls. 

- [ibhagwan's picom](https://aur.archlinux.org/packages/picom-ibhagwan-git/) for
  the compositor. Allows for tyrone144's dual kawase blur, as well as sdhand's
  rounded corners. 

The above list is by no means exhaustive, and I've likely forgotten some things.

## Keybindings
| Primary Keybinding | Secondary Keybinding | Title | Description |
|--------------------------------------------------|-------------------------------------|------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| <kbd>Super-Shift-H</kbd> |  | Show Help Document | Opens a document showing keybinds |
| <kbd>Super-Enter</kbd> |  | Dropdown Terminal | Launches XFCE4 Dropdown Terminal |
| <kbd>Super-Space</kbd> |  | Program Launcher | Launches ulauncher |
| <kbd>Super-Tab</kbd> |  | Toggle Notification Center | Opens/Closes the notification center |
| <kbd>Super-D</kbd> |  | Program Launcher (Dmenu) | Launches Dmenu |
| <kbd>Super-F2</kbd> |  | Web Browser | Launches Google Chrome |
| <kbd>Super-F3</kbd> |  | File Explorer | Launches Nemo |
| <kbd>Super-&lt;Period&gt;</kbd> |  | Text Replacements | Runs texpander script to allow for text replacements |
| <kbd>PrtSc</kbd> |  | Take Screenshot | Runs flameshot to take a screenshot |
| <kbd>Super-PrtSc</kbd> |  | Take Screen Recording | Runs the screen recording script provided to take a screen recording |
| <kbd>Super-P</kbd> |  | Projection Menu | Laptop Screen Only, Mirror, Extend Up, Extend Down, Extend Left, Extend Right, Desk Setup, Custom (Launch arandr) |
| <kbd>Super-Ctrl-B</kbd> |  | Settings Menu | System Monitor, Log Viewer, Settings Application |
| <kbd>Super-Ctrl-Q</kbd> |  | Quit Window/Application | Uses kill to close the focused window |
| <kbd>Super-Shift-X</kbd> |  | Force Quit Window/Application | Uses xkill to close the focused window |
| <kbd>Super-Shift-Space</kbd> |  | Toggle Float | Switches the focused container between floating and tiled |
| <kbd>Super-Shift-&lt;Plus&gt;</kbd> |  | Change to tiled / floating windows | Toggles the focus between tiled windows and floating windows |
| <kbd>Super-Shift-&lt;Minus&gt;</kbd> |  | Move window to scratchpad | Moves the focused window to the scratchpad |
| <kbd>Super-&lt;Minus&gt;</kbd> |  | Show scratchpad | Shows the window in the scratchpad as a floating window. If there are multiple scratchpad windows, cycles through them. |
| <kbd>Super-Shift-S</kbd> |  | Toggle Sticky Float | Toggles between normal floating mode, and sticky mode, which allows floating windows to stick to the screen across other workspaces. |
| <kbd>Super-F</kbd> |  | Toggle Fullscreen | Toggles the focused container between fullscreen and windowed mode |
| <kbd>Super-S</kbd> |  | Stacked Layout | Switches the focused container's layout to stacked |
| <kbd>Super-W</kbd> |  | Tabbed Layout | Switches the focused container's layout to tabbed |
| <kbd>Super-E</kbd> |  | Splitscreen Layout | Switches the focused container's layout to tiled/split. If layout is already tiled/split, toggles between a horizontal / vertical split instead.  |
| <kbd>Super-Q</kbd> |  | Toggle horizontal/vertical split | Toggles the direction of the split (horizontal / vertical) |
| <kbd>Super-B</kbd> |  | Go to last workspace | Switches back and forth between the 2 last used workspaces |
| <kbd>Super-Shift-B</kbd> |  | Move window to last workspace | Moves the focused window back and forth between the 2 last used workspaces |
| <kbd>Super-Ctrl-&leftarrow;</kbd> |  | Go to left workspace | Switches to the workspace to the left of the current one |
| <kbd>Super-Ctrl-&rightarrow;</kbd> |  | Go to right workspace | Switches to the workspace to the right of the current one |
| <kbd>Super-⟨k⟩</kbd> |  | Go to workspace `⟨k⟩` | Switches to workspace `⟨k⟩`, where `⟨k⟩` is the workspace id. |
| <kbd>Super-Ctrl-⟨k⟩</kbd> |  | Send window to workspace `⟨k⟩` | Moves the focused wondow to workspace `⟨k⟩`, where `⟨k⟩` is the workspace id. |
| <kbd>Super-Shift-⟨k⟩</kbd> |  | Carry window to workspace `⟨k⟩` | Moves the focused wondow to workspace `⟨k⟩` AND switches to workspace `⟨k⟩`, where `⟨k⟩` is the workspace id. |
| <kbd>Super-Alt-&uparrow;</kbd> |  | Move workspace up | Moves the current workspace to the display above the current one. |
| <kbd>Super-Alt-&downarrow;</kbd> |  | Move workspace down | Moves the current workspace to the display below the current one. |
| <kbd>Super-Alt-&leftarrow;</kbd> |  | Move workspace left | Moves the current workspace to the display left of the current one. |
| <kbd>Super-Alt-&rightarrow;</kbd> |  | Move workspace right | Moves the current workspace to the display right of the current one. |
| <kbd>Super-&leftarrow;</kbd> | <kbd>Super-J</kbd> | Change to left window | Focuses the window to the left of the currently focused one |
| <kbd>Super-&downarrow;</kbd> | <kbd>Super-K</kbd> | Change to lower window | Focuses the window below the currently focused one |
| <kbd>Super-&uparrow;</kbd> | <kbd>Super-L</kbd> | Change to upper window | Focuses the window above the currently focused one |
| <kbd>Super-&rightarrow;</kbd> | <kbd>Super-;</kbd> | Change to right window | Focuses the window to the right of the currently focused one |
| <kbd>Super-Shift-&leftarrow;</kbd> | <kbd>Super-Shift-J</kbd> | Move window left | Moves the focused window to the left |
| <kbd>Super-Shift-&downarrow;</kbd> | <kbd>Super-Shift-K</kbd> | Move window down | Moves the focused window down |
| <kbd>Super-Shift-&uparrow;</kbd> | <kbd>Super-Shift-L</kbd> | Move window up | Moves the focused window to the up |
| <kbd>Super-Shift-&rightarrow;</kbd> | <kbd>Super-Shift-;</kbd> | Move window right | Moves the focused window to the right |
| <kbd>Super-R</kbd> |  | Resize window | Allows the user to resize the focused window with the arrow keys |
| <kbd>Super-T</kbd> |  | Kill Compositor | Kills the Compositor, removing window decorations and transparency effects |
| <kbd>Super-Ctrl-T</kbd> |  | Start Compositor | Starts a new Compositor, adding window decoration sand transparency effcts |
| <kbd>Super-Shift-D</kbd> |  | Restart Notification Server | Restarts the notification daemon |
| <kbd>Super-Shift-R</kbd> |  | Restart i3 | Restarts i3 inplace, preserves layout/session |
| <kbd>Super-Shift-C</kbd> |  | Reload i3 config file | Reloads i3 config file |
| <kbd>Super-Shift-E</kbd> |  | Exits i3 | Promps the user to exit i3, then exits if desired. |
<!--
| <kbd>Win</kbd>+<kbd>___</kbd> |  |  |  | 
-->

## Workspaces



The workspaces are setup as follows:

- Workspace <kbd>\~</kbd>: 💬 messaging
- Workspace <kbd>1</kbd>: 🌐 web browser
- Workspace <kbd>2</kbd>: 📁 file browser
- Workspace <kbd>3</kbd>: 📝 editing & file viewing
- Workspace <kbd>4</kbd>: 🖥 virutal machines
- Workspace <kbd>5</kbd>: 🎮 Games
- Workspace <kbd>6</kbd> through <kbd>9</kbd>: free space
- Workspace <kbd>0</kbd>: terminal
