#!/bin/bash

# Print help :D
if [[ $# == 0 ]] || [[ $1 == -h ]] || [[ $1 == --help ]]; then
	cat <<- EOF
	backlight.sh -- A backlight script for the 21st century.
	                backlight.sh will adjust the backlight, 
                  and send an appropriate notification.
	
	backlight.sh [-h | --help] COMMAND
	
	  [-h | --help] -- Show this help dialog.
	  COMMAND       -- the command to execute. 
	                   Must be of the following form:
	                             +n | -n
	                   ... where n is any number. 
	
	  Examples:
	
	  # increase backlight by 10
	  backlight.sh +10
	
	  # decrease backlight by 10
	  backlight -10
	EOF
  exit 0
fi

# Perform Action
[[ $1 == +* ]] && xbacklight -inc "${1#+}"
[[ $1 == -* ]] && xbacklight -dec "${1#-}"

# Brightness Icon Ranges
# high   = [100-66)
# medium = [66-33)
# low    = [33-0)
# off    = 0
lvl=$(xbacklight)
lvl="$(printf '%.0f' $lvl)"

if (( $lvl > 66 )); then icon=display-brightness-high-symbolic
elif (( $lvl > 33 )); then icon=display-brightness-medium-symbolic
elif (( $lvl > 0 )); then icon=display-brightness-low-symbolic
else icon=display-brightness-off-symbolic
fi

# Send notification. Requires 2 variables:
#   $lvl  -- The volume level (displayed in title)
#   $icon -- The icon to display
# TODO: support sliders
notify-send.py "Brightness: $lvl" \
               --replaces-process backlightnotif \
               -a 'Backlight Control' -i display-brightness \
               --hint "string:image-path:$icon" \
                      "int:has-percentage:$lvl" \
                      'bool:transient:true' &
