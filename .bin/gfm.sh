#!/bin/bash

if [[ $# == 0 ]] || [[ $1 == '-h' ]] || [[ $1 == '--help' ]]; then
  cat <<-EOF
	USAGE: gfm.sh [ OPTIONS ] <markdown file>
	
	Converts Gitlab Flavored Markdown into PDF or HTML format
	
	DEFAULT OPTIONS: gfm.sh -html --file <markdown file>
	
	FORMAT OPTIONS:
	  -pdf           Converts the GFM to pdf
	  -html          Converts the GFM to html
	
	OUTPUT OPTIONS:
	  -p --print     Prints the generated output     
	  -f --file      Creates a file of the same name
	EOF
  exit 0
fi

# Set defaults
filetype='html'
output='file'

# Process Arguments
let "n = $#"
while [[ $n > 1 ]]; do
  case $1 in
    -pdf|-html) filetype="${1/-/}" ;;
    -p|--print|-f|--file) 
      output="${1/-/}" ;;
    --)
      shift 1
      break ;;
  esac
  let "n--"
  shift 1
done

# Run script
if [[ $output == file ]]; then
  case $filetype in
    html)
      MERMAID_THEME='neutral'             \
      KROKI_DIAGRAM_BLACKLIST=mermaid     \
      pandoc --from markdown --to html5   \
         --self-contained --standalone    \
         --filter pandoc-kroki            \
         --filter pandoc-mermaid          \
         --lua-filter gitlab-math.lua     \
         --lua-filter fix-links.lua       \
         --katex --template=GitHub.html5  \
         -o "$(echo -n "$1" | sed 's/.r\?md/.html/')" "$1"
      ;;
    pdf)
      MERMAID_THEME='neutral'             \
      pandoc --from markdown --to latex   \
        --self-contained --standalone     \
        --filter pandoc-plantuml          \
        --filter pandoc-mermaid           \
        --lua-filter gitlab-math.lua      \
        --lua-filter fix-links.lua        \
        --katex --template=eisvogel       \
        -o "$(echo -n "$1" | sed 's/.r\?md/.pdf/')" "$1"
      ;;
  esac
else
  case $filetype in
    html)
      MERMAID_THEME='neutral'             \
      KROKI_DIAGRAM_BLACKLIST=mermaid     \
      pandoc --from markdown --to html5   \
         --self-contained --standalone    \
         --filter pandoc-kroki            \
         --filter pandoc-mermaid          \
         --lua-filter gitlab-math.lua     \
         --lua-filter fix-links.lua       \
         --katex --template=GitHub.html5  \
         "$1"
         ;;
    pdf)
      MERMAID_THEME='neutral'             \
      pandoc --from markdown --to latex   \
        --self-contained --standalone     \
        --filter pandoc-plantuml          \
        --filter pandoc-mermaid           \
        --lua-filter gitlab-math.lua      \
        --lua-filter fix-links.lua        \
        --katex --template=eisvogel       \
        "$1"
        ;;
  esac
fi
