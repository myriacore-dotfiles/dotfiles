#!/bin/bash

# Print help :D
if [[ $# == 0 ]] || [[ $1 == -h ]] || [[ $1 == --help ]]; then
	cat <<- EOF
	volume.sh -- A volume script for the 21st century.
	             volume.sh will adjust the volume, and
	             send an appropriate notification.
	
	volume.sh [-h | --help] COMMAND
	
	  [-h | --help] -- Show this help dialog.
	  COMMAND       -- the command to execute. 
	                   Must be of the following form:
	                           +n | -n | 0
	                   ... where n is any number. 
	
	  Examples:
	
	  # increase volume by 10
	  volume.sh +10
	
	  # decrease volume by 10
	  volume.sh -10
	
	  # toggle mute
	  volume.sh 0
	EOF
  exit 0
fi

# Send notification. Requires 2 variables:
#   $lvl  -- The volume level (displayed in title)
#   $icon -- The icon to display
notify () {
  # TODO: support sliders (interaction)
  if [[ $lvl != Muted ]]; then
    notify-send.py "Volume: $lvl" \
                   --replaces-process volumenotif \
                   -a 'Volume Control' -i preferences-sound \
                   --hint "string:image-path:$icon" \
                          "int:has-percentage:$lvl" \
                          'bool:transient:true' &
  else
    notify-send.py "Volume: $lvl" \
                   --replaces-process volumenotif \
                   -a 'Volume Control' -i preferences-sound \
                   --hint "string:image-path:$icon" \
                          'bool:transient:true' &
  fi
}

# Perform Action
[[ $1 == 0 ]] && pulsemixer --toggle-mute
( [[ $1 == +* ]] || [[ $1 == -* ]] ) && pulsemixer --change-volume $1

# Brightness Icon Ranges
# high   = [+100-66)
# medium = [66-33)
# low    = [33-0)
# muted  = 0/muted

lvl=$(pulsemixer --get-volume | cut -d ' ' -f 1)
mute=$(pulsemixer --get-mute)

# Notify: Volume Changed
if [[ $1 != 0 ]]; then
  if (( $lvl > 66 )); then icon=volume-level-high
  elif (( $lvl > 33 )); then icon=volume-level-medium
  elif (( $lvl > 0 )); then icon=volume-level-low
  else icon=volume-level-muted
  fi
  notify

# Notify: Mute Toggled
else
  if [[ $mute == 1 ]]; then # Mute on
    icon=volume-level-muted
    lvl=Muted
  else                      # Mute off
    if (( $lvl > 66 )); then icon=volume-level-high
    elif (( $lvl > 33 )); then icon=volume-level-medium
    elif (( $lvl > 0 )); then icon=volume-level-low
    else icon=volume-level-muted
    fi
  fi  
  notify
fi
