#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Launch user-requested polybar
echo "---" | tee -a /tmp/polybar.log
polybar "$1" >>/tmp/polybar.log 2>&1 &
echo "Bar '$1' launched."
